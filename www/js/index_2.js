var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
        navigator.splashscreen.show();
    },
    receivedEvent: function(id) {


        $('.tab-2').css({"display":"none"});

        $('.tabs').find('a').click(function(e) {

            tabId = $(this).attr('data-tab');

            $('.tab').css({"display":"none"});
            $('.tab-'+tabId).css({"display":"block"});

            e.preventDefault();
        })

        apiUrl = "http://7525d0b2.ngrok.io/ktp_app_db/";

        var parentElement = document.getElementById(id);
        var receivedElement = parentElement.querySelector('.received');

        receivedElement.innerHTML = 'Device Model: '    + device.model    + '<br />' +
                                    'Device Cordova: '  + device.cordova  + '<br />' +
                                    'Device Platform: ' + device.platform + '<br />' +
                                    'Device UUID: '     + device.uuid     + '<br />' +
                                    'Device Version: '  + device.version  + '<br />';

        $('#device_uuid').val(device.uuid);
        $('#submit_btn').click(function(e) {
            data = $("#user_data").serialize();
            e.preventDefault();
            $.ajax({
                url: apiUrl+"add_user.php", 
                type: "GET",
                data: data,
                success: function(data, response) {
                    console.log("all good");
                    $('.tab-2').html('<p class=\"worked\">That worked. Here\'s Another screen.</p>');
                },
                error: function(response) {
                    alert("failed");
                }
            });
        })

        $.ajax({
            url: apiUrl+"check_user.php",
            type: "GET",
            data: "device_uuid="+device.uuid,
            success: function(data, response) {
                var data = $.parseJSON(data);
                device_uuid = data[0].device_uuid;
                if(device_uuid == device.uuid) {
                    $('#students').val(data[0].students);
                    $('#level').val(data[0].level);
                }
            },
            error: function(response) {
                alert("Failed to connect to server. Please try again later.");
            }
        });



    }
};






