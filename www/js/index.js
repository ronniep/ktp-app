var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
        navigator.splashscreen.show();
    },
    receivedEvent: function(id) {

		apiUrl = "http://a2bbf34e.ngrok.io/ktp_app_db/";


		$('#device_uuid').val(device.uuid);
		$('#submit_btn').click(function(e) {
		    data = $("#user_data").serialize();
		    e.preventDefault();
		    $.ajax({
		        url: apiUrl+"add_user.php", 
		        type: "GET",
		        data: data,
		        success: function(data, response) {
		            $('.screen-2').animate({"opacity":"0"},500, function(e) {
		            	$(this).css({"display":"none"});
		            	$('.screen-3').addClass('move_view');
		            })
		            
		        },
		        error: function(response) {
		            alert("failed");
		        }
		    });
		})

		$.ajax({
		    url: apiUrl+"check_user.php",
		    type: "GET",
		    data: "device_uuid="+device.uuid,
		    success: function(data, response) {
		        var data = $.parseJSON(data);
		        device_uuid = data[0].device_uuid;
		        if(device_uuid == device.uuid) {
		            $('#students').val(data[0].students);
		            $('#level').val(data[0].level);
		            student_year = data[0].level;
		        }
		    },
		    error: function(response) {
		        alert("Failed to connect to server. Please try again later.");
		    }
		});




		var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
		var d = new Date();
		var n = d.getMonth();

		monthName = months[n];
		console.log(monthName);

		// read data 
		$.get(apiUrl+"data/data.json", function( data ) {
			console.log(data);
			i = 0;
			m = 0;
			data = data;
			console.log(data['title']);
			console.log(data['years']);
			console.log(data['desc']);
			console.log(data['sub']);


			$('.title_screen_title').text(data['title']);
			$('.title_screen_years').text(data['years']);
			$('.title_screen_desc').text(data['desc']);
			$('.title_screen_sub').text(data['sub']);


			while(i <= data['year'].length-1) {
				if(data['year'][i]['name'] == student_year) {
					m = 0;

					$('.year_screen_title').text(data['year'][i]['name']);
					$('.year_screen_desc').text(data['year'][i]['desc']);

					while(m <= data['year'][i]['month'].length-1) {
						$('.year_timeline').append('<div class=\"group\"><div class=\"month\">'+data['year'][i]['month'][m]['month']+'</div><div class=\"icon icon_'+data['year'][i]['month'][m]['icon']+'\"></div><div class=\"title\">'+data['year'][i]['month'][m]['title']+'</div><div class=\"desc\">'+data['year'][i]['month'][m]['desc']+'</div></div>');
						m++;
                        if(m >= data['year'][i]['month'].length-1) {

                            $('.loading').animate({"opacity":"1"},1200, function() {
                                $('.loading').addClass('move_up');
                            });

                        }
					}
				}
				i ++;
			}
		});

		$('.next').click(function(e) {
			$('.screen-1').addClass('move_up');
			$('.screen-2').addClass('move_view');
			e.preventDefault();
		});



		// wheel

		deg = 0;
		minimum = 1000;
		maximum = 5000;
		speed = Math.floor(Math.random() * (maximum - minimum + 1)) + minimum;
		
		function spin() {
			if(speed >= .1) {
				deg-=speed;
				speed-=.05*(speed/5);
				document.getElementById("circle").style.WebkitTransform = "rotate("+deg+"deg)"; 
				requestAnimationFrame(spin);
			} else {
				rotation = $('#circle').css("transform");
				correctRotation(rotation);
			}
		}
		spin();

		function correctRotation(rotation) {
			var values = rotation.split('(')[1].split(')')[0].split(',');
			var a = values[0];
			var b = values[1];
			var c = values[2];
			var d = values[3];
			var scale = Math.sqrt(a*a + b*b);
			var sin = b/scale;
			var angle = Math.round(Math.atan2(b, a) * (180/Math.PI));
		}




    }
};






